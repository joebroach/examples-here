"""Lookup HERE speed data for Metro and PORTAL count locations

Note: This script was written as a separate step for initial development,
  but the hourly aggregation could happen here without any loss of detail.
  The intermediate spliced output file could then be omitted.

"""

import csv
import os
import gzip
import time
import datetime
import pytz
import dateutil  # new in py3 has neat datetime parser built in

read_dir = 'I:/Replica-Data-Stage/HERE/HERE_2017_Q4/'
HERE_TMC_table = (r'I:\Replica-Data-Stage\HERE\supporting-data'
  r'\NPMRDS_TMC_LUT_2016Q2.csv')
link_file = (r'I:\Replica-Data-Stage\HERE\supporting-data'
  r'\metro-TMC-lookup-2017Q4.csv')
crosswalk_outfile = (r'I:\Replica-Data-Stage\HERE\intermediate-outputs'
  r'\metro-TMC-lookup-2017Q4.csv')
splice_file = (r'I:\Replica-Data-Stage\HERE\intermediate-outputs'
  r'\here_metro_2017Q4.csv')

# load in the HERE links to lookup
print('Loading local links...')
links = {}
with open(link_file) as f:
    reader = csv.DictReader(f)
    for r in [r for r in reader if len(r['here_id']) > 0]:
        here_id = r['here_id']
        here_dir = r['here_dir']
        # list since possible that here links repeat (but unlikely)
        if (here_id, here_dir) not in links:
            links[(here_id, here_dir)] = []
        links[(here_id, here_dir)].append(r)
print('Read {} locations and {} here links'
  .format(sum([len(v) for k, v in links.items()]), len(links)))

# Add TMCs from the crosswalk table
print('Looking up TMC codes...')
n = 0
tmc_set = set()
with open(HERE_TMC_table) as f:
    reader = csv.DictReader(f)
    for r in [r for r in reader if (r['LINK_ID'], r['DIR']) in links]:
        for link in links[(r['LINK_ID'], r['DIR'])]:
            link['tmc_id'] = r['TMC']
            n += 1
            tmc_set.add(r['TMC'])
print('Matched TMC codes for {} locations'.format(n))

# Save crosswalk info
print('Saving TMC > Local crosswalk file to {}'.format(crosswalk_outfile))
with open(crosswalk_outfile, 'w') as out:
    fieldnames = list(links[list(links)[0]][0])
    writer = csv.DictWriter(out, fieldnames=fieldnames,
      lineterminator='\n')
    writer.writeheader()
    for k, link_list in links.items():
        for link in link_list:
            #print(k)
            #print(d)
            writer.writerow(link)

# pull relevant records and store as intermediate files
#fieldnames = [k for k, v in links[list(links)[0]][0].items()]
with gzip.open(read_dir + \
  [f for f in os.listdir(read_dir) if f.endswith('.gz')][0], 'rt') as f:
    reader = csv.reader(f)
    fieldnames = next(reader)
    fieldnames.append('start_time')

with open(splice_file, 'w') as out:
    writer = csv.DictWriter(out, fieldnames=fieldnames,
      lineterminator='\n')
    writer.writeheader()

for f_name in sorted([f for f in os.listdir(read_dir) if f.endswith('.gz')]):
    rows = []
    with gzip.open(read_dir + f_name, 'rt') as f:
        print('Reading {} ...'.format(f_name))
        reader = csv.DictReader(f)
        t0 = time.time()
        for row in [r for r in reader if r['source_id'] in tmc_set]:
            # go ahead and adjust time so we can sort later
            start = dateutil.parser.parse(row['utc_time_id'])
            start = pytz.timezone('UTC').localize(start)
            start = start.astimezone(pytz.timezone("America/Los_Angeles"))
            row['start_time'] = start
            rows.append(row)
        print('{} rows retrieved in {:.1f}s'.format(len(rows),
                                                    time.time() - t0))
    rows.sort(key=lambda r: (r['source_id'], r['start_time']))
    with open(splice_file, 'a') as out:
        writer = csv.DictWriter(out, fieldnames=fieldnames,
          lineterminator='\n')
        writer.writerows(rows)
print('Script exited as normally as it ever does!')

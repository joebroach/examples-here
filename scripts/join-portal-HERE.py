"""Join PORTAL stations with nearest HERE link"""

import csv
import psycopg2

outfile = ('I:/Replica-Data-Stage/HERE/supporting-data/'
  'PORTAL-HERE-TMC-crosswalk.csv')

def nearest_link(conn, point_geom, here_table, max_candidates=100, n=1):
    """Return nearest link(s) to given point

    """
    cur = conn.cursor()
    query = ("with index_query as (select st_distance(geom, %s) as dist,"
             "link_id as here_id from {} "
             "where RAMP='N' "
             "order by geom <#> %s "
             "limit %s) "
             "select dist, here_id "
             "from index_query order by dist limit 1;"
             .format(here_table))
    data = [point_geom, point_geom, max_candidates]
    #print(cur.mogrify(query, data))
    cur.execute(query, data)
    match_dst, id = cur.fetchone()
    return match_dst, id


here_table = 'data."here-2016Q2-network-metro"'
conn = psycopg2.connect(host='localhost', database='scratch', user='postgres',
                        port=5439)
cur = conn.cursor()

print('Matching stations to HERE links...')
query = """
  SELECT stationid, geom, highwayid
  FROM data.stations_mp_districts
  """
cur.execute(query)
stations = cur.fetchall()
matchlist = []
matchset = set()
for station_id, point_geom, highwayid in stations:
    match_dst, here_id = nearest_link(conn, point_geom, here_table,
                                      max_candidates=100)
    matchlist.append((str(station_id), str(here_id), str(highwayid),
                      match_dst))
    matchset.add(str(here_id))
print('Matched {} stations'.format(len(matchlist)))

HERE_TMC_table = (r'I:\Replica-Data-Stage\HERE\supporting-data'
                  r'\NPMRDS_TMC_LUT_2016Q2.csv')
# Add TMCs from the crosswalk table
print('Looking up TMC codes...')
tmctable = {here_id: [] for here_id in matchset}
with open(HERE_TMC_table) as f:
    reader = csv.DictReader(f)
    for r in [r for r in reader if r['LINK_ID'] in matchset]:
        tmctable[r['LINK_ID']].append((r['TMC'], r['DIR']))
print('Found TMC candidates for {} links'.format(len(tmctable)))

# Read highway direction data
highway_file = (r'I:\Replica-Data-Stage\PORTAL\supporting-data'
                r'\highway_metadata.csv')
# read in some highway data
highways = {}
with open(highway_file) as f:
    reader = csv.DictReader(f)
    for r in reader:
        highway_id = r['highwayid']
        if highway_id not in highways:
            highways[highway_id] = r
        else:
            print('Ignoring multiple entries for highway {} dir {}'
              .format(highway_id, r['direction']))
print('Read in {} highway-directions'.format(len(highways)))

# choose direction by heuristic
matched = []
for station_id, here_id, highwayid, match_dst in matchlist:
    station_dir = highways[highwayid]['direction']
    # I-84/SR500 are reversed from standard
    if ((highwayid != '8' and highwayid != '53')
        and (station_dir in ['WEST', 'NORTH']
             or highwayid == '7'
             or highwayid == '52')):
        # print(station_dir)
        tmcs = [{'tmc': tmc, 'here_dir': here_dir} for tmc, here_dir in
                tmctable[here_id] if tmc[3] == 'P']
        if len(tmcs) == 1:
            matched.append((station_id, here_id, highwayid,
                            match_dst, tmcs[0]['tmc'], tmcs[0]['here_dir']))
        else:
            print('{} matching TMCs found for {}'.format(len(tmcs),
                                                         station_id))
    elif (station_dir in ['EAST', 'SOUTH']
          or highwayid == '8'
          or highwayid == '53'):
        tmcs = [{'tmc': tmc, 'here_dir': here_dir} for tmc, here_dir in
                tmctable[here_id] if tmc[3] == 'N']
        if len(tmcs) == 1:
            matched.append((station_id, here_id, highwayid,
                            match_dst, tmcs[0]['tmc'], tmcs[0]['here_dir']))
        else:
            print('{} matching TMCs found for {}'.format(len(tmcs),
                                                         station_id))

# Fix some match errors due to ODOT milepost GIS issues
matched.append(('1104', '19365240', '4', -9, '114N04386', 'T'))  # GIS drift
matched.append(('10417', '19363868', '51', -9, '114P14046', 'F'))  # highway dir wrong?
matched.append(('3125', '1000288976', '7', -9, '114P14032', 'F'))  # "ramp"
matched.append(('10421', '21323857', '52', -9, '114N12060', 'T'))  # overlaying links
matched.append(('10400', '19363865', '50', -9, '114N04367', 'T'))  # highway dir wrong?
matched.append(('10432', '21323857', '53', -9, '114N07680', 'F'))  # overlaying links
matched.append(('5210', '786329321', '502', -9, '114N04475', 'T'))  # GIS didn't snap

# Write out the new crosswalk file
print('Writing crosswalk file to {}'.format(outfile))
with open(outfile, "w") as out:
    writer = csv.writer(out, lineterminator='\n')
    writer.writerow(['station_id', 'here_id', 'highwayid', 'match_dst', 'tmc',
                     'here_dir'])
    writer.writerows(sorted(matched))

####################
[(station_id, here_id, highwayid, match_dst) for
  station_id, here_id, highwayid, match_dst in matchlist if station_id=='5210']
matched[:10]

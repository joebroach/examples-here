"""Lookup HERE speed data for Metro and PORTAL count locations

Note: This script was written as a separate step for initial development,
  but the hourly aggregation could happen here without any loss of detail.
  The intermediate spliced output file could then be omitted.

"""

import csv
import os
import gzip
import time
import datetime
import pytz
import dateutil  # new in py3 has neat datetime parser built in

read_dir = 'I:/Replica-Data-Stage/HERE/HERE_2018_Q4/'
link_file = (r'I:\Replica-Data-Stage\HERE\supporting-data'
  r'\PORTAL-HERE-TMC-crosswalk.csv')
splice_file = (r'I:\Replica-Data-Stage\HERE\intermediate-outputs'
  r'\here_portal_2018Q4.csv')

# load in the HERE links to lookup
print('Loading links...')
n = 0
tmc_set = set()
with open(link_file) as f:
    reader = csv.DictReader(f)
    for r in [r for r in reader if len(r['tmc']) > 0]:
        n += 1
        tmc_set.add(r['tmc'])
print('Read {} TMC codes for {} stations'.format(len(tmc_set), n))

# pull relevant records and store as intermediate files
#fieldnames = [k for k, v in links[list(links)[0]][0].items()]
with gzip.open(read_dir + \
  [f for f in os.listdir(read_dir) if f.endswith('.gz')][0], 'rt') as f:
    reader = csv.reader(f)
    fieldnames = next(reader)
    fieldnames.append('start_time')

with open(splice_file, 'w') as out:
    writer = csv.DictWriter(out, fieldnames=fieldnames,
      lineterminator='\n')
    writer.writeheader()

for f_name in sorted([f for f in os.listdir(read_dir) if f.endswith('.gz')]):
    rows = []
    with gzip.open(read_dir + f_name, 'rt') as f:
        print('Reading {} ...'.format(f_name))
        reader = csv.DictReader(f)
        t0 = time.time()
        for row in [r for r in reader if r['source_id'] in tmc_set]:
            # go ahead and adjust time so we can sort later
            start = dateutil.parser.parse(row['utc_time_id'])
            start = pytz.timezone('UTC').localize(start)
            start = start.astimezone(pytz.timezone("America/Los_Angeles"))
            row['start_time'] = start
            rows.append(row)
        print('{} rows retrieved in {:.1f}s'.format(len(rows),
                                                    time.time() - t0))
    rows.sort(key=lambda r: (r['source_id'], r['start_time']))
    with open(splice_file, 'a') as out:
        writer = csv.DictWriter(out, fieldnames=fieldnames,
          lineterminator='\n')
        writer.writerows(rows)
print('Script exited as normally as it ever does!')

"""Aggregate 5-min HERE data to hourly from spliced TMC files

Note - Input table MUST be sorted by time for each link id

"""

import datetime
import pytz
import csv
import dateutil  # new in py3 has neat datetime parser built in
import time

# set some variables
infile = (r'I:\Replica-Data-Stage\HERE\intermediate-outputs'
  r'\here_portal_2018Q4.csv')
outfile = (r'I:\Replica-Data-Stage\HERE\intermediate-outputs'
  r'\here_portal_2018Q4_hourly.csv')

print('Grouping 5-min speed data as hourly...')
rows = {}
t1 = time.time()
with open(infile) as f:
    reader = csv.DictReader(f)
    for r in reader:
        # convert time from utc to local PST/PDT
        #start = dateutil.parser.parse(r['utc_time_id'])
        #start = pytz.timezone('UTC').localize(start)
        #start = start.astimezone(pytz.timezone("America/Los_Angeles"))
        start = dateutil.parser.parse(r['start_time'])
        end = start + datetime.timedelta(hours=1)
        tmc_id = r['source_id']
        try:
            speed = float(r['avg_speed'])
        except ValueError:
            continue
        key = (tmc_id, start.year, start.month, start.day,
          start.hour)
        if key not in rows:
            rows[key] = {
              'tmc_id': tmc_id,
              'start_time': start.isoformat(),
              'end_time': end.isoformat(),
              'speed': []
              }
        rows[key]['speed'].append(speed)
print('{:.0f}s'.format(time.time() - t1))

bad_row_list = [v for k, v in rows.items() if len(v['speed']) != 12]
row_list = [v for k, v in rows.items() if len(v['speed']) == 12]
row_list.sort(key=lambda v: (v['tmc_id'], v['start_time']))
print('{} rows out of {} with unexpected number valid observations will '
      'be ignored!'.format(len(bad_row_list), len(rows)))
[r for r in row_list if r['tmc_id'] == '114N12998' and r['start_time'].startswith('2017-05-01T')]

print('Averaging speeds...')
for r in row_list:
    r['average_speed_kmh'] = sum(r['speed']) / len(r['speed'])
    del(r['speed'])

with open(outfile, "w") as out:
    writer = csv.DictWriter(out, fieldnames=row_list[0].keys(),
      lineterminator='\n')  # lineterminator avoids blank lines in Windows
    print('Writing downsampled file')
    writer.writeheader()
    writer.writerows(row_list)
print('Script exited as normally as it ever does!')
